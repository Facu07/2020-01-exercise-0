
package ar.uba.fi.tdd.exercise;

class GildedRose {
    Item[] items;

    static final int MAX_QUALITY = 50;
    static final int SECOND_WEEK_BEFORE = 10;
    static final int FIRST_WEEK_BEFORE = 5;
    static final int DEGRADATION_CONJURED_ITEM = 2;
    static final int NORMAL_DEGRADATION = 1;
    static final int NORMAL_AGING = 1;
    static final int MIN_QUALITY = 0;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void maximumOfQualityReached(Item item){
        if (item.quality > MAX_QUALITY) {
            item.quality = MAX_QUALITY;
        }
    }

    public void minimumOfQualityReached(Item item){
        if (item.quality < MIN_QUALITY) {
            item.quality = MIN_QUALITY;
        }
    }

    public void isSellInBetweenOneAndFive(Item item){
        if (item.sellIn <= FIRST_WEEK_BEFORE && item.sellIn > MIN_QUALITY) {
            item.quality = item.quality + 2;
        }
    }

    public void isSellInBetweenFiveAndTen(Item item){
        if (item.sellIn >= FIRST_WEEK_BEFORE && item.sellIn <= SECOND_WEEK_BEFORE) {
            item.quality = item.quality + NORMAL_AGING;
        }
    }

    public void afterConcertPasses(Item item){
        if(item.sellIn < 1){
            item.quality = MIN_QUALITY;
        }
    }

    public void degrading_x2_AfterSellInPassed(Item item){
        if (item.sellIn < MIN_QUALITY) {
            item.quality = item.quality - NORMAL_DEGRADATION;
        }
    }

    public void updateQuality() {

        for (int i = 0; i < items.length; i++) {

                String itemName = items[i].Name;
                switch (itemName) {

                    case "Aged Brie":
                        items[i].quality = items[i].quality + NORMAL_AGING;
                        maximumOfQualityReached(items[i]);
                        break;

                    case "Backstage passes to a TAFKAL80ETC concert":
                        isSellInBetweenOneAndFive(items[i]);
                        isSellInBetweenFiveAndTen(items[i]);
                        items[i].quality = items[i].quality + NORMAL_AGING;
                        maximumOfQualityReached(items[i]);
                        afterConcertPasses(items[i]);
                        items[i].sellIn = items[i].sellIn - 1;
                        break;

                    case "Sulfuras, Hand of Ragnaros":
                        break;

                    case "Conjured":
                        items[i].quality = items[i].quality - DEGRADATION_CONJURED_ITEM;
                        items[i].sellIn = items[i].sellIn - 1;
                        minimumOfQualityReached(items[i]);
                        break;

                    default:
                        items[i].quality = items[i].quality - NORMAL_DEGRADATION;
                        items[i].sellIn = items[i].sellIn - 1;
                        degrading_x2_AfterSellInPassed(items[i]);
                        minimumOfQualityReached(items[i]);
                        break;
                }
        }
    }
}
