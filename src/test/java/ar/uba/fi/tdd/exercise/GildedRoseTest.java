package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
		Item[] items = new Item[] { new Item("fixme", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat("fixme").isEqualTo(app.items[0].Name);
	}

	@Test
	public void DegradingQuality_x2_OnceExpired(){
		Item[] items = new Item[] { new Item("fixme", 0, 4) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(2).isEqualTo(app.items[0].quality);
	}

	@Test
	public void QualityNeverNegative(){
		Item[] items = new Item[] { new Item("fixme", 0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-1).isLessThan(app.items[0].quality);
	}

	@Test
	public void AgedBrieIncreasesQuality(){
		Item[] items = new Item[] { new Item("Aged Brie", 10, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(1).isEqualTo(app.items[0].quality);
	}

	@Test
	public void MaximumQualityReached(){
		Item[] items = new Item[] { new Item("Aged Brie", 10, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(51).isGreaterThan(app.items[0].quality);
	}

	@Test
	public void LegendaryItem_Quality_SellIn_NeverDecreaces(){
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 50) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(49).isLessThan(app.items[0].quality);
		assertThat(9).isLessThan(app.items[0].sellIn);
	}

	@Test
	public void BackstagepassesQuality_MoreThan10DaysBeforeSellIn(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(11).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagepassesQuality_10DaysBeforeSellIn(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 9, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(12).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagepassesQuality_5DaysBeforeSellIn(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 4, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(13).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagepassesQualityAfterSellIn(){
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredItemsQualityDrops_x2(){
		Item[] items = new Item[] { new Item("Conjured", 10, 10) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredItemsQualityNotNegative(){
		Item[] items = new Item[] { new Item("Conjured", 10, 1) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-1).isLessThan(app.items[0].quality);
	}
}
